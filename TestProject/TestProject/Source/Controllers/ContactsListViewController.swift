//
//  ContactsListViewController.swift
//  TestProject
//
//  Created by Dariya on 11/19/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

final class ContactsListViewController: UIViewController {
    
    private let titlesString = "Messages"
    private let segmentControlTitles = ["Public", "Private"]
    private let sideSpacing: CGFloat = 27.0
    private let upperMenuSideSpacing: CGFloat = 13.0
    
    private let tableView = UITableView()
    private let upperMenu = UIView()
    private let underlineSegmentedControl = UnderlineSegmentController()
    private var upperMenuHeight: CGFloat {
        switch UIDevice().type {
        case .iPhone5, .iPhone5C, .iPhone5S, .iPhoneSE:
            return 109.0
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8, .iPhone6Splus, .iPhone6plus, .iPhone7plus, .iPhone8plus:
            return 119.0
        default:
            return 145.0
        }
    }
    
    private var calls: [PhoneCallModel] = []
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        reloadData()
    }
    
    // MARK: - data
    private func reloadData() {
        calls = CallsListDataManager.getLastCalls()
        tableView.reloadData()
    }
    
    // MARK: - UI
    private func setupUI() {
        view.backgroundColor = UIColor.white
        setupTableView()
        setupUpperMenu()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.setupSideConstraintsWithMargins(left: sideSpacing, right: -sideSpacing, topMargin: upperMenuHeight)
        tableView.register(ContactsListTableViewCell.self, forCellReuseIdentifier: "\(ContactsListTableViewCell.self)")
        tableView.estimatedRowHeight = 2.0
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
    }
    
    private func setupUpperMenu() {
        // container for menu
        view.addSubview(upperMenu)
        upperMenu.setupSideConstraintsWithMargins(left: sideSpacing, right: -sideSpacing, topMargin: 0.0, bottomMargin: nil)
        upperMenu.heightAnchor.constraint(equalToConstant: upperMenuHeight).isActive = true
        
        // menu button
        let menuButton = UIButton()
        menuButton.setImage(UIImage(named: "menuIcon"), for: .normal)
        upperMenu.addSubview(menuButton)
        menuButton.setupSideConstraintsWithMargins(left: nil, right: -upperMenuSideSpacing, topMargin: nil, bottomMargin: -55.0)
        
        // search button
        let searchButton = UIButton()
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        searchButton.setImage(UIImage(named: "searchIcon"), for: .normal)
        upperMenu.addSubview(searchButton)
        searchButton.rightAnchor.constraint(equalTo: menuButton.leftAnchor, constant: -21.0).isActive = true
        searchButton.centerYAnchor.constraint(equalTo: menuButton.centerYAnchor).isActive = true
        
        // titles
        let titles = UILabel()
        titles.text = titlesString
        titles.font = UIFont.titleLSizeBoldAppFont
        upperMenu.addSubview(titles)
        titles.setupSideConstraintsWithMargins(left: upperMenuSideSpacing, right: 0.0, topMargin: nil, bottomMargin: nil)
        titles.bottomAnchor.constraint(equalTo: searchButton.bottomAnchor).isActive = true
        titles.textAlignment = .left
        
        // segmentedControl
        upperMenu.addSubview(underlineSegmentedControl)
        underlineSegmentedControl.setupSideConstraintsWithMargins(left: upperMenuSideSpacing, right: nil, topMargin: nil, bottomMargin: -15.0)
        underlineSegmentedControl.widthAnchor.constraint(equalToConstant: 125.0).isActive = true
        underlineSegmentedControl.heightAnchor.constraint(equalToConstant: 21.0).isActive = true
        underlineSegmentedControl.elements = segmentControlTitles
        underlineSegmentedControl.delegate = self
    }
    
}

// MARK: - UITableViewDataSource
extension ContactsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ContactsListTableViewCell.self)", for: indexPath) as? ContactsListTableViewCell else { return UITableViewCell() }
        cell.setupWith(model: calls[indexPath.row])
        return cell
    }
}

// MARK: - UnderlineSegmentControllerDelegate
extension ContactsListViewController: UnderlineSegmentControllerDelegate {
    func segmentedControlDidSelectetElement(at index: Int) {
        //TODO: add filtering logic
    }
}
