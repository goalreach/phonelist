//
//  CallsListDataManager.swift
//  TestProject
//
//  Created by Dariya on 11/19/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

final class CallsListDataManager {
    static func getLastCalls() -> [PhoneCallModel] {
        var phoneCalls: [PhoneCallModel] = []
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Mercedez Benz 365 AMG", type: .call, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Opel Astra", type: .headphones, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "BMW X5 2017", type: .image, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Mercedez Benz 365 AMG", type: .video, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Opel Astra", type: .headphones, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Opel Astra", type: .call, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Mercedez Benz 365 AMG", type: .image, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Opel Astra", type: .video, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "BMW X5 2017", type: .call, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "BMW X5 2017", type: .image, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "Mercedez Benz 365 AMG", type: .video, time: Date()))
        phoneCalls.append(PhoneCallModel(userName: "Joshua Max", userImage: UIImage(named: "testImage")!, carName: "BMW X5 2017", type: .headphones, time: Date()))
        return phoneCalls
    }
}
