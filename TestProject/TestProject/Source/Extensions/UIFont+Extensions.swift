//
//  UIFont+Extensions.swift
//  TestProject
//
//  Created by Dariya on 11/20/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

fileprivate let proximaNovaSemibold = "ProximaNova-Semibold"
fileprivate let proximaNovaBold = "ProximaNova-Bold"

extension UIFont {
    static func appFont(size: CGFloat) -> UIFont {
        return UIFont(name: proximaNovaSemibold, size: size)!
    }
    static func boldAppFont(size: CGFloat) -> UIFont {
        return UIFont(name: proximaNovaSemibold, size: size)!
    }
    
    static var xsSizeAppFont: UIFont {
        return appFont(size: 12.0)
    }
    static var sSizeAppFont: UIFont {
        return appFont(size: 14.0)
    }
    static var mSizeAppFont: UIFont {
        return appFont(size: 16.0)
    }
    static var lSizeAppFont: UIFont {
        return appFont(size: 18.0)
    }
    static var titleLSizeBoldAppFont: UIFont {
        return boldAppFont(size: 24.0)
    }

}
