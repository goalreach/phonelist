//
//  UIColor+Extensions.swift
//  TestProject
//
//  Created by Dariya on 11/19/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

extension UIColor {
    static let tpGreyColor = UIColor(red: 0.67, green: 0.71, blue: 0.75, alpha: 1.0)
    static let tpLightGreyColor = UIColor(red: 0.76, green: 0.76, blue: 0.76, alpha: 1.0)
    static let tpBlackColor = UIColor(red: 0.17, green: 0.17, blue: 0.17, alpha: 1.0)
    static let tpBlueColor = UIColor(red: 0.11, green: 0.52, blue: 1.0, alpha: 1.0)
}
