//
//  Layout+Extensions.swift
//  TestProject
//
//  Created by Dariya on 11/19/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    public func setupSideConstraintsWithMargins(left leftMargin: CGFloat? = 0.0, right rightMargin: CGFloat? = 0.0, topMargin: CGFloat? = 0.0, bottomMargin: CGFloat? = 0.0) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        if let leftMargin = leftMargin {
            self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: leftMargin).isActive = true
        }
        if let rightMargin = rightMargin {
            self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: rightMargin).isActive = true
        }
        if let topMargin = topMargin {
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: topMargin).isActive = true
        }
        if let bottomMargin = bottomMargin {
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: bottomMargin).isActive = true
        }
    }
}
