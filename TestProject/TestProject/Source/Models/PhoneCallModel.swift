//
//  PhoneCallModel.swift
//  TestProject
//
//  Created by Dariya on 11/19/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

enum PhoneCallType {
    case image
    case call
    case video
    case headphones
    
    var image: UIImage? {
        switch self {
        case .image:
            return UIImage(named: "photoIcon")
        case .call:
            return UIImage(named: "phoneReciever")
        case .video:
            return UIImage(named: "cameraIcon")
        case .headphones:
            return UIImage(named: "headPhones")
        }
    }
    
    var titile: String? {
        switch self {
        case .image:
            return "Image"
        case .call:
            return "Missed call"
        case .video:
            return "Missed video chat"
        case .headphones:
            return "Audio"
        }
    }
    
}

final class PhoneCallModel {
    var userName: String?
    var userImage: UIImage?
    var carName: String?
    var type: PhoneCallType = .image
    var time: Date?
    var isOnline: Bool = true
    
    // MARK: - init
    init(userName: String, userImage: UIImage, carName: String, type: PhoneCallType, time: Date) {
        self.userName = userName
        self.userImage = userImage
        self.carName = carName
        self.type = type
        self.time = time
    }
}
