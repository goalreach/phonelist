//
//  ContactsListTableViewCell.swift
//  TestProject
//
//  Created by Dariya on 11/19/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

class ContactsListTableViewCell: UITableViewCell {

    private let containerView = UIView()
    private let carNameLabel = UILabel()
    private let userImageView = UserOnlineImageView()
    private let timeLabel = UILabel()
    private let clientNameLabel = UILabel()
    private let callStatusLabel = UILabel()
    private let callStatusImage = UIImageView()
    
    // MARK: - init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    // MARK: - data
    func setupWith(model: PhoneCallModel) {
        carNameLabel.text = model.carName
        userImageView.image = model.userImage
        clientNameLabel.text = model.userName
        callStatusImage.image = model.type.image
        callStatusLabel.text = model.type.titile
    }
    
    // MARK: - ui
    private func setupUI() {
        self.selectionStyle = .none
        setupContainerView()
        setupCardNameLabel()
        setupImageView()
        setupTimeLabel()
        setupClientNameLabel()
        setupCallStatusUIImageView()
        setupCallStatusLabel()
    }
    
    private func setupContainerView() {
        contentView.addSubview(containerView)
        containerView.backgroundColor = UIColor.white
        containerView.setupSideConstraintsWithMargins(topMargin: 5.0, bottomMargin: -30)
        containerView.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        containerView.layer.cornerRadius = 2.0
        containerView.layer.masksToBounds = false
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        containerView.layer.shadowRadius = 10.0
        containerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.05).cgColor
        containerView.layer.shadowOpacity = 1.0
    }
    
    private func setupCardNameLabel() {
        let labelBackgroundView = UIView()
        labelBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(labelBackgroundView)
        labelBackgroundView.topAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        labelBackgroundView.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        labelBackgroundView.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        labelBackgroundView.leftAnchor.constraint(greaterThanOrEqualTo: containerView.leftAnchor, constant: 0.0).isActive = true
        labelBackgroundView.backgroundColor = UIColor.tpGreyColor
        labelBackgroundView.layer.cornerRadius = 2.0
        labelBackgroundView.layer.maskedCorners = [CACornerMask.layerMaxXMaxYCorner, CACornerMask.layerMinXMaxYCorner]
        
        labelBackgroundView.addSubview(carNameLabel)
        carNameLabel.font = UIFont.xsSizeAppFont
        carNameLabel.textColor = UIColor.white
        carNameLabel.setupSideConstraintsWithMargins(left: 18.0, right: -18.0)
    }
    
    private func setupImageView() {
        containerView.addSubview(userImageView)
        userImageView.setupSideConstraintsWithMargins(left: 10.0, right: nil, topMargin: 10.0, bottomMargin: -10.0)
        userImageView.widthAnchor.constraint(equalTo: userImageView.heightAnchor).isActive = true
    }
    
    private func setupTimeLabel() {
        containerView.addSubview(timeLabel)
        timeLabel.textAlignment = .right
        timeLabel.setupSideConstraintsWithMargins(left: nil, right: -14.0, topMargin: 28.0, bottomMargin: -28.0)
        timeLabel.font = UIFont.xsSizeAppFont
        timeLabel.textColor = UIColor.tpLightGreyColor
        timeLabel.text = "7.22 pm"
    }
    
    private func setupClientNameLabel() {
        containerView.addSubview(clientNameLabel)
        clientNameLabel.setupSideConstraintsWithMargins(left: nil, right: nil, topMargin: 19.0, bottomMargin: -33.0)
        clientNameLabel.leftAnchor.constraint(equalTo: userImageView.rightAnchor, constant: 30.0).isActive = true
        clientNameLabel.textColor = UIColor.tpBlackColor
        clientNameLabel.font = UIFont.mSizeAppFont
    }
    
    private func setupCallStatusLabel() {
        containerView.addSubview(callStatusLabel)
        callStatusLabel.translatesAutoresizingMaskIntoConstraints = false
        callStatusLabel.leftAnchor.constraint(equalTo: callStatusImage.rightAnchor, constant: 6.0).isActive = true
        callStatusLabel.centerYAnchor.constraint(equalTo: callStatusImage.centerYAnchor).isActive = true
        callStatusLabel.textColor = UIColor.tpLightGreyColor
        callStatusLabel.font = UIFont.xsSizeAppFont
    }
    
    private func setupCallStatusUIImageView() {
        containerView.addSubview(callStatusImage)
        callStatusImage.translatesAutoresizingMaskIntoConstraints = false
        callStatusImage.leftAnchor.constraint(equalTo: clientNameLabel.leftAnchor).isActive = true
        callStatusImage.topAnchor.constraint(equalTo: clientNameLabel.bottomAnchor, constant: 6.0).isActive = true
        callStatusImage.heightAnchor.constraint(equalToConstant: 11.0).isActive = true
    }
}
