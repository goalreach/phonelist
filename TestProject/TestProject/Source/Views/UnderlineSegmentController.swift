//
//  UnderlineSegmentController.swift
//  TestProject
//
//  Created by Dariya on 11/20/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit

protocol UnderlineSegmentControllerDelegate: class {
    func segmentedControlDidSelectetElement(at index: Int)
}

final class UnderlineSegmentController: UIView {
    
    private let stackView = UIStackView()
    private var buttons: [UIButton] = []
    private let selectedLine = UIView()
    private var leftSpaceLineConstraint: NSLayoutConstraint?
    private var widthLineConstraint: NSLayoutConstraint?
    
    weak var delegate: UnderlineSegmentControllerDelegate?
    var elements: [String] = [] {
        didSet {
            updateButtons()
        }
    }
    var selectedIndex: Int = 0 {
        didSet {
            updateSelectedLinePosition()
        }
    }
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    // MARK: - actions
    @objc func didPressButton(button: UIButton) {
        selectedIndex = button.tag
        delegate?.segmentedControlDidSelectetElement(at: selectedIndex)
    }

    // MARK: - UI
    private func setupUI() {
        setupStackView()
        setupSelectedLine()
    }
    
    private func setupStackView() {
        stackView.axis = .horizontal
        addSubview(stackView)
        stackView.setupSideConstraintsWithMargins(bottomMargin: 2.0)
        stackView.distribution = .equalSpacing
        stackView.spacing = 31.0
    }
    
    private func setupSelectedLine() {
        selectedLine.backgroundColor = UIColor.tpBlueColor
        addSubview(selectedLine)
        selectedLine.setupSideConstraintsWithMargins(left: nil, right: nil, topMargin: nil, bottomMargin: 0.0)
        selectedLine.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        widthLineConstraint = selectedLine.widthAnchor.constraint(equalToConstant: 0.0)
        widthLineConstraint?.isActive = true
        leftSpaceLineConstraint = selectedLine.leftAnchor.constraint(equalTo: leftAnchor)
        leftSpaceLineConstraint?.isActive = true
        
    }
    
    private func updateButtons() {
        buttons.removeAll()
        stackView.removeAllArrangedSubviews()
        for index in 0 ..< elements.count {
            let button = UIButton()
            button.titleLabel?.font = UIFont.mSizeAppFont
            button.setTitleColor(.black, for: .normal)
            button.setTitle(elements[index], for: .normal)
            button.tag = index
            button.addTarget(self, action: #selector(didPressButton(button:)), for: .touchUpInside)
            stackView.addArrangedSubview(button)
            buttons.append(button)
        }
        updateSelectedLinePosition()
    }
    
    private func updateSelectedLinePosition() {
        layoutIfNeeded()
        guard buttons.count > selectedIndex else { return }
        let selectedButtonFrame = buttons[selectedIndex].frame
        widthLineConstraint?.constant = selectedButtonFrame.width
        leftSpaceLineConstraint?.constant = selectedButtonFrame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
}
