//
//  UserOnlineImageView.swift
//  TestProject
//
//  Created by Dariya on 11/20/18.
//  Copyright © 2018 dariyaprokopovych. All rights reserved.
//

import UIKit
import QuartzCore

class UserOnlineImageView: UIView {
    
    private let imageView = UIImageView()
    private let onlineImageView = UIImageView()
    
    var image: UIImage? {
        didSet {
            imageView.image = image
            setupMask()
        }
    }

    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupMask()
    }
    
    // MARK: - UI
    private func setupUI() {
        setupImageView()
        setupMask()
        setupOnlineImage()
    }
    
    private func setupImageView() {
        addSubview(imageView)
        imageView.setupSideConstraintsWithMargins()
        imageView.layer.cornerRadius = 2.0
        imageView.layer.masksToBounds = true
    }
    
    private func setupOnlineImage() {
        onlineImageView.image = UIImage(named: "onlineImage")
        addSubview(onlineImageView)
        onlineImageView.setupSideConstraintsWithMargins(left: nil, right: 0.0, topMargin: nil, bottomMargin: 0.0)
    }
    
    private func setupMask() {
        let mask = CALayer()
        mask.contents =  UIImage(named: "maskImage")?.cgImage as Any
        mask.frame = CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height)
        imageView.layer.mask = mask
        imageView.layer.masksToBounds = true
    }
}
